#include <gtest/gtest.h>
#include "world.h"

TEST(WorldTest, BoilerPlateGTest)
{
    ASSERT_EQ(worldMessage(), "World !");
}

TEST(WorldTestFails, BoilerPlateGTest)
{
    ASSERT_EQ(worldMessage(), "");
}
