project(world)

add_library(world SHARED src/world.cpp)

add_subdirectory(test)

target_include_directories(world PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)