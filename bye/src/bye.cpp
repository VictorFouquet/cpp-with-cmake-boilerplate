#include <iostream>
#include "bye.h"

void sayBye()
{
    std::cout << "Bye ";
    sayWorld();
}

std::string byeMessage() 
{
    std::string str = "Bye " + worldMessage();
    return str;
}