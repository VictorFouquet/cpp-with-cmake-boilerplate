#ifndef BYE_H
#define BYE_H

#include "world.h"
#include <string>

void sayBye();

std::string byeMessage();

#endif