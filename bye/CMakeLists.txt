project(bye)

add_library(bye SHARED src/bye.cpp)

add_subdirectory(test)

target_include_directories(bye PUBLIC
    ${CMAKE_CURRENT_SOURCE_DIR}/include
)

target_link_libraries(bye world)