#include <gtest/gtest.h>
#include "bye.h"

TEST(ByeTest, BoilerPlateGTest)
{
    ASSERT_EQ(byeMessage(), "Bye World !");
}

TEST(ByeTestFails, BoilerPlateGTest)
{
    ASSERT_EQ(byeMessage(), "");
}