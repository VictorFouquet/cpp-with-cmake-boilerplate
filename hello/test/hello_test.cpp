#include <gtest/gtest.h>
#include "hello.h"

TEST(HelloTest, BoilerPlateGTest)
{
    ASSERT_EQ(helloMessage(), "Hello World !");
}

TEST(HelloTestFails, BoilerPlateGTest)
{
    ASSERT_EQ(helloMessage(), "");
}
