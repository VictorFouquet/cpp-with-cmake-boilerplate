#ifndef HELLO_H
#define HELLO_H
#include "world.h"
#include <string>

void sayHello();

std::string helloMessage();

#endif