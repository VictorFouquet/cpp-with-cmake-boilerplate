#include <gtest/gtest.h>
#include "greet.h"

TEST(GreetTest, BoilerPlateGTest)
{
    ASSERT_EQ(greetMessage(), "Hello World !Bye World !");
}

TEST(GreetTestFails, BoilerPlateGTest)
{
    ASSERT_EQ(greetMessage(), "");
}