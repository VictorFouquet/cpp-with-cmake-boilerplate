#include "greet.h"

void greet()
{
    sayHello();
    sayBye();
}

std::string greetMessage()
{
    std::string str = helloMessage() + byeMessage();
    return str;
}