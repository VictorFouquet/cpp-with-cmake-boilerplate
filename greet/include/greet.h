#ifndef GREET_H
#define GREET_H

#include <iostream>
#include "hello.h"
#include "bye.h"

void greet();

std::string greetMessage();

#endif