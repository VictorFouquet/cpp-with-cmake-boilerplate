# cpp with cmake and gtest boilerplate

Simple C++ boiler-plate with googletest and CMake.

# prerequisites

CMake version ^3.21

# build and run the project

To compile, pull the repo, create a build folder at the root of the project,
open a terminal in the created build folder and run :

```cmake .. && make```

To run the example from the same directory after compilation:

```./boiler_plate```

Unit tests can be run individually or all at once.

Note each test file includes a successful and failing test, meant to prove the libs are actually tested.

To run all the tests, from the build directory :

```./all_tests```

To run the tests individually :

```./bye_test```

```./greet_test```

```./hello_test```

```./world_test```
